# Приходишь в Армянский музей, а там роботы в нарды играют
### You come to an Armenian museum, and there are robots playing backgammon

An artwork diaplyed at the [4th Annual Installation Art Festival](https://www.accea.info/feeling/) "Feeling".

The festival was organized by an Armenian center for contemporary experimental art (NPAK), Yerevan, Armenia.
Curator: Giorgio Granata.

#### [Photos and Videos from the exhibition](./exhibition/)

## Plate

Have you heard anecdotes about Armenians playing backgammon in unexpected locations? 
This artwork is yet another joke. 
How exciting is their game to you? 
Are you eager to know who wins this match?


## Tecnical Details

Intel NUC, Debian 12, xfce.

```sh
sudo apt-get install python3-pip python3-tk python3-dev gnome-screenshot
python3 -m venv .venv
. .venv/bin/activate
pip install -r requirements.txt

sudo cp bg.service /etc/systemd/system/bg.service
sudo systemctl daemon-reload
sudo systemctl enable bg.service
sudo service bg start
```

