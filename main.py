from subprocess import Popen
import pyautogui
import pywinctl
import pymonctl
import time
import signal
from random import randrange


def main():
    global gnubg, unclutter
    time.sleep(5)
    unclutter = Popen(['/usr/bin/unclutter', '-idle', '1', '-root'])
    gnubg = Popen(['/usr/games/gnubg'])
    time.sleep(3)
    pyautogui.PAUSE = 1
    pyautogui.FAILSAFE = False
    mon = pymonctl.getAllMonitors()[0]
    screen = mon.workarea
    win = pywinctl.getWindowsWithTitle('GNU Backgammon')[0]
    win.restore()
    win.resizeTo(screen.right-30, screen.bottom-55)
    while True:
        win.moveTo(randrange(20), randrange(20))
        pyautogui.moveTo(screen.right-1, screen.bottom-1)
        time.sleep(1)
        pyautogui.hotkey('ctrl', 'n')

        # field_length = pyautogui.locateCenterOnScreen('length7.png', grayscale=True, minSearchTime=2)
        # pyautogui.doubleClick(field_length)
        # pyautogui.write('1')  # 1 game per match (less buttons to press)

        pyautogui.click(find_ok())  # start game
        pyautogui.click(find_ok())  # start game now for real!!
        time.sleep(1)
        pyautogui.moveTo(0, 50)
        
        while True:
            time.sleep(1)
            try:
                btn_ok = find_ok()
            except pyautogui.ImageNotFoundException:
                pass
            else:
                pyautogui.click(btn_ok)  # finish game
                break


def find_ok():
    btn_ok = None
    try:
        btn_ok = pyautogui.locateCenterOnScreen('ok.png', grayscale=True, minSearchTime=2)
    except pyautogui.ImageNotFoundException:
        btn_ok = pyautogui.locateCenterOnScreen('ok2.png', grayscale=True)
    return btn_ok


def handler_stop_signals(signum, frame):
    global gnubg, unclutter
    print("Gracefully quitting...", flush=True)
    gnubg.terminate()
    unclutter.terminate()
    exit()


if __name__ == '__main__':
    signal.signal(signal.SIGINT, handler_stop_signals)
    signal.signal(signal.SIGTERM, handler_stop_signals)
    main()
